def get_cs():
	return input("Input:\n")

def cs_to_lot(cs):
	return [tuple(x.split("=")) for x in cs.split(";")]

def lot_to_cs(lot):
	lst = [x[0]+"="+x[1] for x in lot if True]
	cs = ""
	for x in lst:
		cs = cs + x + ";"
	return cs[0:-1]

def main():
	cs=get_cs()
	lot=cs_to_lot(cs) # convert connect string to list of tuples
	print(lot)
	cs=lot_to_cs(lot)
	print(cs)


main()