def get_cs():
    return input("Input:\n")
    
def cs_to_dict(cs):
    list = cs.split(';')
    d = {x.split("=")[0]:x.split("=")[1] for x in list if True}
    return d

def dict_to_cs(d):
    cs = ""
    for i in d:
        cs = cs + i + "=" + d[i] + ";"
    return cs[0:-1]

def main():
    cs=get_cs()
    d=cs_to_dict(cs) # convert connect string to a dictionary
    print(d)
    cs=dict_to_cs(d)
    print(cs)

main()